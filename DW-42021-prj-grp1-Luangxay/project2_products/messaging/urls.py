from django.urls import path
from . import views


urlpatterns = [
    path("", views.messaging.as_view(), name='messaging'),
    path('<str:room_name>/', views.GetRoom.as_view(), name='room'),

]