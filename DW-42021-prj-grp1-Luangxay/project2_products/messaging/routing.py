# messaging/routing.py
from django.urls import re_path


from . import consumers

# link a websocket to a url(?)
websocket_urlpatterns = [
    re_path(r'ws/messaging/(?P<room_name>\w+)/$', consumers.dmConsumer.as_asgi()),
]
