# messaging/views
from django.shortcuts import render
from django.views.generic import TemplateView
from django.views.generic import ListView
from django.contrib.auth.models import User
from .models import SavedChat
from django.http import HttpResponseRedirect
from .forms import MsgForm
from administration.models import Profile


# Create your views here.
# Displays the "homepage" of messaging/ inbox
class messaging(ListView):
    template_name = "messaging/messaging_home.html"
    queryset = User.objects.all()
    context_object_name = 'users'

    # fetches all the users, and sets the user's notifications to 0
    def get(self, request):
        profileobj = Profile.objects.get(user_id=request.user.id)
        profileobj.notifications = 0
        profileobj.save()

        return render(request, self.template_name, {self.context_object_name: self.queryset, 'current': request.user})


# Displays chatroom between a specific user
class GetRoom(ListView):
    template_name = "messaging/message_page.html"
    queryset = User.objects.all()
    context_object_name = 'users'

    # fetches all the users, room name, chat room form, and user/receiver and sends it to the html
    def get(self, request, room_name):
        userobj = request.user
        receiver = self.getReceiver(room_name, userobj)
        form = MsgForm()
        savedmsg = SavedChat.objects.all().filter(chatRoom=room_name).order_by('timeSent')

        return render(request, self.template_name,
                      {'room_name': room_name, self.context_object_name: self.queryset, 'current': userobj,
                       'savedmsg': savedmsg, 'form': form, 'receiver': receiver})

    # when the user submits a message, remake the same page.
    def post(self, request, room_name):
        userobj = request.user
        receiver = self.getReceiver(room_name, userobj)
        form = MsgForm()
        savedmsg = SavedChat.objects.all().filter(chatRoom=room_name).order_by('timeSent')

        return render(request, self.template_name,
                      {'room_name': room_name, self.context_object_name: self.queryset, 'current': request.user,
                       'savedmsg': savedmsg, 'form': form, 'receiver': receiver})

    # Get the User object of the receiver base on the chat room
    # a chat room format is id1_id2, id1 being <= id2 , and one of the id corresponds to the current user id
    def getReceiver(self, room_name, userobj):

        userid = str(userobj.id)

        ids = room_name.split('_')
        id1 = ids[0]
        id2 = ids[1]

        receiverobj1 = User.objects.get(id=id1)
        receiverobj2 = User.objects.get(id=id2)

        # If the id1 is not equal to current user id
        if id1 != userid:
            return receiverobj1
        # If the id2 is not equal to the current user id
        elif id2 != userid:
            return receiverobj2
        # if both id corresponds to the userid then the user is talking in their own personal dm
        else:
            return userobj;


