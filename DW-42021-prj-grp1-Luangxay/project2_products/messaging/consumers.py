# chat/consumers.py
import json
import os
import django

from asgiref.sync import sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer
from .models import SavedChat
from administration.models import Profile


# for async live communications + adds to database
class dmConsumer(AsyncWebsocketConsumer):
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'rest.settings')
    os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"
    django.setup()

    # Saves the SavedChat object in the database, and adds + 1 to the notification field of the reciever
    def saveToDB(self, msg, receiver):
        # create a new message in the database
        msg.save()

        # add a notification
        profileobj = Profile.objects.get(user_id=receiver)
        currentcounter = profileobj.notifications
        newcounter = currentcounter + 1
        profileobj.notifications = newcounter
        profileobj.save()

    # Makes live chat available
    async def connect(self):
        self.chatroom_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.chatroom_name

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    # Closes the chat
    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        chatroom = text_data_json['chatRoom']
        receiver = text_data_json['receiver']

        # Creates a temporary SavedChat object with the information, and send to saveToDB to be saved
        tempmsg = SavedChat(chatRoom=chatroom, savedMessages=message)
        self.saveToDB(tempmsg, receiver)

        # Send message to chat room
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message
            }
        )

    # Receive message from chat room
    async def chat_message(self, event):
        message = event['message']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message

        }))
