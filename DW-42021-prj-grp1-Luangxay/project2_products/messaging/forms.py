from django import forms


# Form for the chat field
class MsgForm(forms.Form):
    chat_message_input = forms.CharField(max_length=1000, label='')
