from django.db import models
from django.utils import timezone


# # Create your models here.
# Model for saved chat messages
class SavedChat(models.Model):

    savedMessages = models.CharField(max_length=1000)
    chatRoom = models.CharField(max_length=120)
    timeSent = models.DateTimeField(default=timezone.now)

    # returns the name of the chat room
    def __str__(self):
        temp = "room: " + self.chatRoom
        return temp
