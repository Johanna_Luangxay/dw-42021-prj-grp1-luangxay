from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.urls import reverse

# Choices for status of the item
STATUS_CHOICES = (
    ('new','New'),
    ('used','Used')
)

# Choices for the genre of the item
GENRE_CHOICES = (
    ('food', 'Food'),
    ('book', 'Books'),
    ('clothing', 'Clothing'),
    ('electronics', 'Electronics'),
    ('instrument', 'Instrument')
)


# Item model, with all the relevant information to a product being sold
class Item(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    itemname = models.CharField(max_length=150)
    genre = models.CharField(max_length=100, choices=GENRE_CHOICES)
    description = models.TextField(max_length=300, default='Describe your item.')
    price = models.DecimalField(max_digits=10, decimal_places=2)
    status = models.CharField(max_length=4, choices=STATUS_CHOICES)
    date_posted = models.DateTimeField(default=timezone.now)
    likes = models.ManyToManyField(User, related_name='product_item')
    flagged = models.ManyToManyField(User, related_name='flagged_item')
    itempic = models.ImageField(upload_to='item_pic/', default='item_pic/item_default.PNG')

    # Redirecting to the url after the item is created, override
    def get_absolute_url(self):
        return reverse('item-detail', kwargs={'pk':self.pk})

    # Returns the total number of likes a specific item has
    def getTotalLikes(self):
        return self.likes.count()


# Comment associated to an Item model
class ItemComment(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=25, default="")
    comment = models.TextField(max_length=300, default="")
    rating = models.DecimalField(max_digits=2, decimal_places=1, default=2.5, validators=[MinValueValidator(0), MaxValueValidator(5)])
    date_posted = models.DateTimeField(default=timezone.now)

    # Redirect after the comment is made
    def get_absolute_url(self):
        return reverse('item-detail', kwargs={'pk':self.item.pk})
