# item/urls.py
from django.urls import path
from . import views
from .views import ItemsListView, ItemCreateView, ItemDetailView, LikeView, ItemUpdateView, ItemDeleteView, SearchItemView, FilterItemView, CommentFormView, FlagView

urlpatterns = [
    path("items/new/", ItemCreateView.as_view(), name="item-create"),
    path("", views.home, name="home"),
    path("items/", ItemsListView.as_view(), name="items"),
    path("items/<int:pk>/", ItemDetailView.as_view(), name="item-detail"),
    path("like/<int:pk>", LikeView, name="like_item"),
    path("items/<int:pk>/update", ItemUpdateView.as_view(), name="item-update"),
    path("items/<int:pk>/delete", ItemDeleteView.as_view(), name="item-delete"),
    path("search/", SearchItemView.as_view(), name="item-search"),
    path("filter/", FilterItemView.as_view(), name="item-filter"),
    path("items/<int:pk>/comment", CommentFormView.as_view(), name="item-comment"),
    path("flag/<int:pk>", FlagView, name="flag_item")
]