#item/views
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.shortcuts import render, get_object_or_404
from django.views.generic import CreateView, ListView, DetailView, UpdateView, DeleteView
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .models import Item, ItemComment
from django.db.models import Avg, Count


# ListView for all of the items, displays all of the items in the database
class ItemsListView(ListView):
    model = Item
    template_name = "item/items.html"
    paginate_by = 10


# CreateView to create an item and add it to the database
class ItemCreateView(LoginRequiredMixin, CreateView):
    model = Item
    fields = ['itemname', 'genre', 'description', 'price', 'status']

    # Checks for form validity, override
    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


# DetailView of each item, shows more detailed information
class ItemDetailView(DetailView):
    model = Item
    template_name = "item/itemDetail.html"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        averagerating = ItemComment.objects.filter(item_id=self.kwargs['pk']).aggregate(Avg('rating'))
        if averagerating.get('rating__avg') is not None:
            context['avgRating'] = round(averagerating.get('rating__avg'), 1)
        else:
            context['avgRating'] = 0
        return context


# Form to update the information about an item, permitted depending on group and owner
class ItemUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Item
    fields = ['itemname', 'genre', 'description', 'price', 'itempic', 'status']

    # User must pass the conditions to be able to edit the item
    def test_func(self):
        item = self.get_object()

        is_owner = self.request.user == item.user
        is_admin_item_gp = str(self.request.user.groups.all()[0]) == "admin_item_gp"
        is_admin_gp = str(self.request.user.groups.all()[0]) == "admin_gp"

        return is_owner or is_admin_gp or is_admin_item_gp


# DeleteView to delete the item from the database, depending on group and owner
class ItemDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Item
    success_url = '/items'

    # User must pass the conditions to be able to delete the item
    def test_func(self):
        item = self.get_object()

        is_owner = self.request.user == item.user
        is_admin_item_gp = str(self.request.user.groups.all()[0]) == "admin_item_gp"
        is_admin_gp = str(self.request.user.groups.all()[0]) == "admin_gp"

        return is_owner or is_admin_gp or is_admin_item_gp


# CreateView to add a comment to an existing item
class CommentFormView(LoginRequiredMixin, CreateView):
    model = ItemComment
    fields = ['title', 'comment', 'rating']
    template_name = "item/item_comment_form.html"

    # Checks form validity, override
    def form_valid(self, form):
        item = get_object_or_404(Item, id=self.kwargs['pk'])
        form.instance.user = self.request.user
        form.instance.item = item
        return super().form_valid(form)


# ListView that shows the search results and shows the ones that match the search query
class SearchItemView(ListView):
    model = Item
    template_name = "item/items.html"

    # Uses the searched term to return the list of items that match the search query
    def get_queryset(self):
        search = self.request.GET.get('search')
        if search:
            result = Item.objects.filter(itemname__icontains=search)
            count = len(result)

            if count > 0:
                messages.success(self.request, "{0} matches".format(count))
            else:
                messages.error(self.request, "no matches")
            return result

        else:
            return Item.objects.all()


# ListView that shows the results of the filter
class FilterItemView(ListView):
    model = Item
    template_name = "item/items.html"

    # Uses the filtered term to return the list of items that match the filter query
    def get_queryset(self):
        filtered = self.request.GET.get('filterby')
        if filtered == "all":
            return Item.objects.all()
        elif filtered == "likes":
            return Item.objects.annotate(count=Count('likes')).order_by('-count')
        else:
            result = Item.objects.filter(genre__icontains=filtered)
            return result


# Function that shows the home page
def home(request):
    return render(request, "item/home.html")


# Function that lets the users like, with each user only being able to like one per item
def LikeView(request, pk):
    item = get_object_or_404(Item, id=request.POST.get('item_id'))
    if request.user in item.likes.all():
        item.likes.remove(request.user)
    else:
        item.likes.add(request.user)
    return HttpResponseRedirect(reverse('item-detail', args=[str(pk)]))


# Function that lets the user flag
def FlagView(request, pk):
    item = get_object_or_404(Item, id=request.POST.get('item_id'))
    if request.user in item.flagged.all():
        item.flagged.remove(request.user)
    else:
        item.flagged.add(request.user)
    return HttpResponseRedirect(reverse('item-detail', args=[str(pk)]))


