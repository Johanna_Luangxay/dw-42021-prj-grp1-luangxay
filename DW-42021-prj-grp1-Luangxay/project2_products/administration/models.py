from django.db import models
from django.contrib.auth.models import User
from django.db.models import DateField, CharField
from django.utils import timezone


class Profile(models.Model):
    """
    Profile model for a user
    Extends django user with One to One field

    Adds: Birthday, Bio, Picture, Flag, and Warn
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    birthday = DateField(auto_now=False, auto_now_add=False)
    address = CharField(max_length=100)
    bio = CharField(max_length=500)
    pic = models.ImageField(upload_to='user_images', default='user_pic/default_profile.jpg')
    flag = models.BooleanField(default=False)
    warn = models.IntegerField(default=0)
    notifications = models.IntegerField(default=0)

    def __str__(self):
        return self.user

    user.username = None
    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []


class Log(models.Model):
    admin_user = models.ForeignKey(User, on_delete=models.CASCADE)
    message = CharField(max_length=500)
    time = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.message
