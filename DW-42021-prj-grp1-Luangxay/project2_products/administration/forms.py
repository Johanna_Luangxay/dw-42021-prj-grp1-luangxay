from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User, Group
from django.forms import NumberInput

from administration.models import Profile


class AdminRegistrationForm(UserCreationForm):
    """
    User Creation form for admin
    User can chose the group when creating a user
    """
    first_name = forms.CharField()
    last_name = forms.CharField()
    email = forms.EmailField()
    group = forms.ModelChoiceField(queryset=Group.objects.all(), required=True)

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'group', 'password1', 'password2']


class AdminEditForm (forms.ModelForm):
    """
    Form to allow admin to modify a user
    """
    first_name = forms.CharField()
    last_name = forms.CharField()
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']


class ProfileEditForm(forms.ModelForm):
    """
    Form to allow admin to modify a profile of a user
    """
    YEARS = [x for x in range(1940, 2021)]
    birthday = forms.DateField(widget=NumberInput(attrs={'type': 'date'}))
    address = forms.CharField()
    bio = forms.CharField()

    class Meta:
        model = Profile
        fields = ['birthday', 'address', 'bio', 'pic']


class ModifyGroupForm(forms.ModelForm):
    """
    Form to allow admin to edit a Group of the user
    """
    group = forms.ModelChoiceField(queryset=Group.objects.all(), required=True)

    class Meta:
        model = Profile
        fields = ['group']