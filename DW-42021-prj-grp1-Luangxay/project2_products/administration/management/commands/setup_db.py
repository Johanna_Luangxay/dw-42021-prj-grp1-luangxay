from django.core.management.base import BaseCommand
from django.contrib.auth.models import Group, User
import os
import random as rnd
from administration.models import Profile
from item.models import Item


class Command(BaseCommand):
    def handle(self, *args, **options):
        """
        Handles the command, calls the other methods
        """
        self.reset_db()
        self.create_groups()

        self.create_superUser()
        self.create_sample_admin_user_gp()
        self.create_sample_admin_item_gp()
        self.create_sample_memebers()
        self.add_sample_items()

    def reset_db(self):
        """
        Deletes entirely the database and recreates a new one
        *note: Only works on windows, does not work on heroku
        """
        os.system('cmd /c "python manage.py reset_db --close-sessions"')
        os.system('cmd /c "python manage.py migrate"')

    def create_groups(self):
        """
        Creates the four groups
        admin_gp, member, admin_user_gp, admin_item_gp
        """
        Group.objects.create(name="admin_gp")
        Group.objects.create(name="member")
        Group.objects.create(name="admin_user_gp")
        Group.objects.create(name="admin_item_gp")

    def create_superUser(self):
        """
        Creates the super user with
            username: nasr
            password: 123
            group: admin_gp
        """
        # Create the super user
        nasr = User.objects.create_user(username="nasr", first_name="Nasreddine", password="123", is_superuser=True,
                                        is_staff=True)

        bruno = User.objects.create_user(username="bruno", first_name="Bruno", last_name="Delgado", password="1234",
                                        email="bruno.delgado@gmail.com", is_superuser=True, is_staff=True )

        # Adding super user to group admin_gp
        group = Group.objects.get(name='admin_gp')
        Profile.objects.create(user=nasr, birthday="2000-07-21", address="7176 Birch Hill Street, Innisfil, ON L9S 1X6")
        nasr.groups.add(group)

        Profile.objects.create(user=bruno, birthday="1998-05-25", address="9628 Space Street, Mars, ON G5E 9Y3")
        bruno.groups.add(group)

    def create_sample_admin_user_gp(self):
        """
        Creates admin_user_gp users
        """
        # Creates the two users
        jo = User.objects.create_user(username="johanna", password="1234", first_name="Johanna", last_name="Luangxay", email="johanna.luangxay@gmail.com")
        Profile.objects.create(user=jo, birthday="2002-07-21", address="8894 Albany Lane, Saint-Félicien, QC G8K 6V2", bio="My favorite color is purple.")

        le = User.objects.create_user(username="le", password="1234", first_name="Le", last_name="Ly", email="le.ly@dawsoncollege.qc.ca")
        Profile.objects.create(user=le, birthday="2000-07-21", address="9354 Edgemont Lane, Ottawa, ON K1G 1X0", bio="I like python")

        # Adding both users to group admin_user_pg
        group = Group.objects.get(name='admin_user_gp')
        jo.groups.add(group)
        le.groups.add(group)

    def create_sample_admin_item_gp(self):
        """
        Creates admin_item_gp users
        """
        # Creates the two users
        ja = User.objects.create_user(username="jackson", password="1234", first_name="Jackson", last_name="Kwan",email="jackson.kwan@emai.com")
        Profile.objects.create(user=ja, birthday="2002-12-23",
                               address="8894 Albany Lane, Saint-Félicien, QC G8K 6V2", bio="I like java")

        ji = User.objects.create_user(username="jimmy", password="1234", first_name="Jimmy", last_name="Le", email="jimmy.ly@email.ca")
        Profile.objects.create(user=ji, birthday="2000-07-21", address="9354 Edgemont Lane, Ottawa, ON K1G 1X0", bio="I like linux")

        # Adding both users to admin_item_gp
        group = Group.objects.get(name='admin_item_gp')
        ja.groups.add(group)
        ji.groups.add(group)

    def create_sample_memebers(self):
        """
        Creates memebers
        """
        # All members have passwords 1234
        global_password = "1234"
        # List of members (username, firstname, lastname, email, dob, address, bio)
        list_members = [("khalil", "Khalil", "Coz", "khalil.cox@email.com", "1992-01-23", "107 Warren, Dr.Delhi, ON N4B 7R6", "I like bears"),
                        ("eileen", "Eileen", "Joyce", "eileen.joyce.emai.com", "1990-03-13", "43 Manhattan Rd.Juan de Fuca Shore, BC V0S 7K7", "I like monkeys"),
                        ("natalie", "Natalie", "Hicks", "natalie.hicks@yahhoo.com", "1982-02-02", "41 N. Center Ave.York, ON M6C 0T7", "I like sleeping"),
                        ("wesley", "Wesley", "Hahn", "wesly.han@gmail.ca", "1991-10-22", "7608 Kent Dr.Innisfail, AB T4G 6P6", "I like the outdoors"),
                        ("lorena", "Lorena", "Kelley", "lorena.kelly@email.com", "1991-10-22", "46 Coffee St.Okotoks, AB T1S 8B5", "I like pineapple"),
                        ("austin", "Austin", "Stanley", "austin.stanley@email.com", "1997-12-21", "2 Sherman Street Nelson, BC V1L 8E8", "I like water"),
                        ("lea", "Lea", "Omnivox", "lea.omnivox@dawsoncollege.qc.ca", "2002-09-07", "4 Sherman Street Nelson, BC V1L 8E8", "I like school"),
                        ]

        group = Group.objects.get(name='member')

        # Loops through the member list and creates all members
        for member in list_members:
            user = User.objects.create_user(username=member[0], password=global_password, first_name=member[1], last_name=member[2], email=member[3])
            Profile.objects.create(user=user, birthday=member[4], address=member[5], bio=member[6])
            user.groups.add(group)

    def add_sample_items(self):
        """
        Adds sample items with random users
        """
        # List of items
        item_list = [("Potato", "food", "Organic potato from my garden", 2.5, "new"),
                     ("Apple", "food", "Fresh apple", 3.0, "used"),
                     ("Nike shoes", "clothing", "shoes, brand", 100, "used"),
                     ("Erlenmeyer", "hardware", "science, fragile", 20.99, "new"),
                     ("Banana", "food", "food, raw", 2.0, "new"),
                     ("Tomato", "food", "food, raw", 2.6, "used"),
                     ("Apple Juice", "food", "food, processed", 4.3, "new"),
                     ("Hats", "clothing", "hat, brand", 50.5, "old"),
                     ("Cucumber", "food", "food, raw", 3.4, "new"),
                     ("Speakers", "electronics", "Big speaker with strong bass", 900, "new"),
                     ("Dress", "clothing", "Pink Summer Dress", 25, "used"),
                     ("Watch", "clothing", "Vintage gold watch", 500, "new"),
                     ("The Little Prince", "books", "Classical Story by Saint-Exupéry", 10.15, "new"),
                     ("Harry Potter", "books", "Volume 2 The Secret Chamber", 25.99, "new"),
                     ("Phone", "electronics", "Apple 4 grey", 250.75, "used"),
                     ("1984", "books", "By Gearge Owell", 15, "new"),
                     ("Sunglasses", "clothing", "UV Sunglasses for men", 56.75, "new"),
                     ("Donut", "food", "food, raw", 3.15, "new"),
                     ("Flute", "instrument", "Professional alto flute", 31.95, "new"),
                     ("Guitar", "instrument", "Acoustic guitar", 112.39, "used"),]

        # Loops through all items and adds a random user
        for item in item_list:
            users = User.objects.all()
            index = rnd.randint(0, len(users)-1)
            random_user = users[index]

            Item.objects.create(user=random_user, itemname=item[0], genre=item[1], description=item[2], price=item[3],
                                status=item[4])

