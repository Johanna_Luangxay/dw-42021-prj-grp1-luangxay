# administration/views
from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import User
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, get_object_or_404
from django.urls import reverse
from django.views.generic import ListView, DeleteView, UpdateView
from shapeshifter.views import MultiModelFormView
from administration.forms import AdminRegistrationForm, AdminEditForm, ProfileEditForm, ModifyGroupForm
from administration.models import Log
from user_management.forms import ProfileRegistrationForm


def has_admin_rights(user):
    """
    Only admin_user_gp, admin_item_gp, and admin_gp groups has admin rights
    """
    is_admin_user_gp = str(user.groups.all()[0]) == "admin_user_gp"
    is_admin_item_gp = str(user.groups.all()[0]) == "admin_item_gp"
    is_admin_gp = str(user.groups.all()[0]) == "admin_gp"

    return is_admin_user_gp or is_admin_gp or is_admin_item_gp


class UserListView(LoginRequiredMixin, UserPassesTestMixin, ListView):
    """
    ListView for User Model
    """
    model = User

    def test_func(self):
        """
        Only people with admin rights can access this page
        """
        return has_admin_rights(self.request.user)


class SearchUserList(LoginRequiredMixin, UserPassesTestMixin, ListView):
    """
    List view for searching a User
    """
    model = User
    template_name = "auth/user_list.html"

    def get_queryset(self):
        """
        Returns the query set of the searched string
        """
        search = self.request.GET.get("user-search")
        # if there is a search
        if search:
            # gets the query set of searched string
            result = User.objects.filter(username__icontains=search)
            count = len(result)

            # if there are matches show the count
            if count > 0:
                messages.success(self.request, "{0} matches".format(count))
            # if there are no matches shows error
            else:
                messages.error(self.request, "no matches")

            return result
        # if there is no search, returns everything
        else:
            return User.objects.all()

    def test_func(self):
        """
        Only people with admin rights can access this page
        """
        return has_admin_rights(self.request.user)


class LogListView(LoginRequiredMixin, UserPassesTestMixin, ListView):
    model = Log

    def test_func(self):
        """
        Only people with admin rights can access this page
        """
        return has_admin_rights(self.request.user)

class DeleteUserView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    """
    View to delete a user
    """
    model = User
    success_url = '/admin/'

    def delete(self, request, *args, **kwargs):
        """
        Overriding delete method to allow logging
        """
        # Gets the object and deletes it
        user = get_object_or_404(User, id=self.kwargs['pk'])
        user.delete()
        # Add log entry
        Log.objects.create(admin_user=self.request.user, message="delete user {0}".format(user))
        return HttpResponseRedirect(self.success_url)

    def test_func(self):
        """
        Only people with admin rights can access this page
        """
        return has_admin_rights(self.request.user)


class RegisterUser(LoginRequiredMixin, UserPassesTestMixin, SuccessMessageMixin, MultiModelFormView):
    """
    View to register a user for admin
    """
    form_classes = (AdminRegistrationForm, ProfileRegistrationForm)
    template_name = "auth/admin_user_register.html"
    success_url = "/admin/"

    def forms_valid(self):
        """
        Checks if the form is valid
        """
        forms = self.get_forms()
        user_form = forms['adminregistrationform']
        profile_form = forms['profileregistrationform']

        # if both forms are valid, saves to database
        if user_form.is_valid() and profile_form.is_valid():
            # gets the user and sets group to what admin specified
            user = user_form.save()
            user.groups.add(user_form.cleaned_data['group'])

            # saves the profile
            profile = profile_form.save(commit=False)
            profile.user = user
            profile.save()

            # Add a log entry
            Log.objects.create(admin_user=self.request.user, message="create new user {0}".format(user))
            return redirect(self.success_url)

    def test_func(self):
        """
        Only people with admin rights can access this page
        """
        return has_admin_rights(self.request.user)


class EditUser(LoginRequiredMixin, UserPassesTestMixin, SuccessMessageMixin, MultiModelFormView):
    """
    View for edit a user for admin
    """
    form_classes = (AdminEditForm, ProfileEditForm)
    template_name = "auth/admin_edit_user.html"
    success_url = "/admin/"

    def forms_valid(self):
        """
        Checks if the form is valid
        """
        forms = self.get_forms()
        user_form = forms['admineditform']
        profile_form = forms['profileeditform']

        # if both forms are valid, saves to database
        if user_form.is_valid() and profile_form.is_valid():
            # gets the user and sets group to what admin specified
            user = user_form.save()

            # saves the profile
            profile = profile_form.save(commit=False)
            profile.user = user
            profile.save()

            # Add a log entry
            Log.objects.create(admin_user=self.request.user, message="update user {0}".format(user))
            return redirect(self.success_url)

    def get_instances(self):
        """
        Gets to instance of profile and user to pre load the form
        """
        user = get_object_or_404(User, id=self.kwargs['pk'])
        profile = user.profile
        instances = {
            'admineditform': user,
            'profileeditform': profile
        }
        return instances

    def test_func(self):
        """
        Only people with admin rights can access this page
        """
        return has_admin_rights(self.request.user)


class ModifyGroup(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    """
    View to modify a user's group
    """
    model = User
    form_class = ModifyGroupForm
    template_name = "auth/admin_edit_user.html"
    success_url = "/admin/"

    def form_valid(self, form):
        """
        Checks if the form is valid
        """
        # Saves the form and sets the newly specified group
        user = form.save()
        user.groups.clear()
        user.groups.add(form.cleaned_data['group'])

        # Add a log entry
        Log.objects.create(admin_user=self.request.user, message="modified group for user {0}".format(user))
        return super().form_valid(form)

    def test_func(self):
        """
        Only people with admin rights can access this page
        """
        return has_admin_rights(self.request.user)


@login_required
@user_passes_test(lambda u: has_admin_rights(u))
def warn_view(request, pk):
    """
    Warns a user. If user has more than 3 warns, the account is disabled
    """
    user = get_object_or_404(User, id=request.POST.get('warn_user'))

    # gets the profile of the user and increments warn by 1
    profile = user.profile
    profile.warn = profile.warn+1
    profile.save()

    # Add a log entry
    Log.objects.create(admin_user=request.user, message="warned user {0}".format(user))

    # if warns is greater than 3, disables the user's account
    if profile.warn > 3:
        user.is_active = False
        user.save()

        # Add a log entry
        Log.objects.create(admin_user=request.user, message="{0} has been banned".format(user))

    return HttpResponseRedirect(reverse("admin_page"))


@login_required
@user_passes_test(lambda u: has_admin_rights(u))
def flag_view(request, pk):
    """
    Flags a user.
    """
    user = get_object_or_404(User, id=request.POST.get('flag_user'))

    profile = user.profile
    # Toggles the flagged value
    if profile.flag:
        profile.flag = False
        # Add a log entry
        Log.objects.create(admin_user=request.user, message="{0} has been flagged".format(user))
    else:
        profile.flag = True
        # Add a log entry
        Log.objects.create(admin_user=request.user, message="{0} has been unflagged".format(user))

    profile.save()

    return HttpResponseRedirect(reverse("admin_page"))




