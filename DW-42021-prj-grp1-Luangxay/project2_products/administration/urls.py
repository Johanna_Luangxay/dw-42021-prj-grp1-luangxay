# administration/urls.py
from django.urls import path

from .views import UserListView, DeleteUserView, RegisterUser, EditUser, ModifyGroup, warn_view, flag_view, \
    SearchUserList, LogListView

urlpatterns = [
    # User List for admin
    path("", UserListView.as_view(), name="admin_page"),
    # Delete a user
    path("user/delete/<int:pk>", DeleteUserView.as_view(), name="user_delete"),
    # Register a user
    path("register", RegisterUser.as_view(), name="admin_user_register"),
    # Edit a user
    path("edit/<int:pk>", EditUser.as_view(), name="admin_edit_user"),
    # Edit the group of the user
    path("edit/group/<int:pk>", ModifyGroup.as_view(), name="admin_modify_group"),
    # Warns the user
    path("warn/<int:pk>", warn_view, name='warn_user'),
    # Flags the user
    path("flag/<int:pk>", flag_view, name='flag_user'),
    # Search a user
    path("search/", SearchUserList.as_view(), name='user_search'),
    path("log/", LogListView.as_view(), name='user_logs')
]
