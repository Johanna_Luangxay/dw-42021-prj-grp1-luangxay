# user_management/views
from django.contrib import messages
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import User, Group
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import redirect
from django.views.generic import DetailView
from user_management.forms import UserRegistrationForm, ProfileRegistrationForm, UserUpdateForm, ProfileUpdateForm
from shapeshifter.views import MultiModelFormView


class RegisterUser(SuccessMessageMixin, MultiModelFormView):
    """
    View for Registering a new user
    Extends MultiModelFormView which allows us to combine two forms into one
    """

    # Combines the UserRegistrationForm and the ProfileRegistrationForm
    form_classes = (UserRegistrationForm, ProfileRegistrationForm)
    template_name = "user_management/registration.html"
    success_url = "/"

    def forms_valid(self):
        """
        Overriding the forms_valid method
        """

        # Gets the 2 forms
        forms = self.get_forms()
        user_form = forms['userregistrationform']
        profile_form = forms['profileregistrationform']

        # if both forms are valid
        if user_form.is_valid() and profile_form.is_valid():
            # Saves the user form
            user = user_form.save()

            # Assigns the group to member (Newly registered user is automatically a member, only admin can change group)
            group = Group.objects.get(name='member')
            user.groups.add(group)

            # Saves the profile form
            profile = profile_form.save(commit=False)
            profile.user = user
            profile.save()

            # Success Message
            messages.success(self.request, "User: {0} has been created. You may login now!".format(user.username))

            # Redirects to home if user registered properly
            return redirect('home')


class UserLoginView(SuccessMessageMixin,LoginView):
    """
    View for User Login
    """
    template_name = 'user_management/login.html'
    success_message = "You have been logged in. Happy Shopping!"


class UserLogoutView(SuccessMessageMixin, LogoutView):
    """
    View for User Logout
    """
    template_name = 'item/home.html'
    success_message = "You have been successfully logged out!"


class UpdateProfile(MultiModelFormView):
    """
    View for Updating user and profile details
    Extends MultiModelFormView which allows us to combine two forms into one
    """

    # Combines the two forms into one UserUpdateForm and ProfileUpdateForm
    form_classes = (UserUpdateForm, ProfileUpdateForm)
    template_name = "user_management/user_form.html"
    success_url = "/"

    def get_instances(self):
        """
        Gets to instance of profile and user to pre load the form
        """
        user = self.request.user
        profile = user.profile
        instances = {
            'userupdateform': user,
            'profileupdateform': profile
        }
        return instances


class ProfileDetailView(DetailView):
    """
    Detail view of a User and Profile
    """
    template_name = "user_management/user_detail.html"
    model = User


class PasswordChangeView(SuccessMessageMixin, PasswordChangeView):
    """
    View to allow user change their password
    """
    template_name = "user_management/change-password.html"
    form_class = PasswordChangeForm
    success_url = "/"
    success_message = "Your password has been changed successfully"

