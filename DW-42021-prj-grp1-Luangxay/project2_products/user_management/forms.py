from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.forms import NumberInput
from administration.models import Profile


class UserRegistrationForm(UserCreationForm):
    """
    User Creation form for unregistered user
    """
    first_name = forms.CharField()
    last_name = forms.CharField()
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password1', 'password2']


class ProfileRegistrationForm(forms.ModelForm):
    """
    Profile Registeration form for unregistered user
    """
    YEARS = [x for x in range(1940, 2021)]
    birthday = forms.DateField(widget=NumberInput(attrs={'type': 'date'}))
    address = forms.CharField()
    bio = forms.CharField()

    class Meta:
        model = Profile
        fields = ['birthday', 'address', 'bio']


class UserUpdateForm(forms.ModelForm):
    """
    User update form for a registered user
    """
    first_name = forms.CharField()
    last_name = forms.CharField()
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']


class ProfileUpdateForm(forms.ModelForm):
    """
    Profile update form for registered user
    """
    YEARS = [x for x in range(1940, 2021)]
    birthday = forms.DateField(widget=NumberInput(attrs={'type': 'date'}))
    address = forms.CharField()
    bio = forms.CharField()

    class Meta:
        model = Profile
        fields = ['birthday', 'address', 'bio', "pic"]

