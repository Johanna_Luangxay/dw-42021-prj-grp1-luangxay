# user_management/urls.py
from django.urls import path

from .views import ProfileDetailView, UpdateProfile, RegisterUser, UserLoginView, UserLogoutView, PasswordChangeView

urlpatterns = [
    # Register a user
    path("register", RegisterUser.as_view(), name="registration"),
    # Login a user
    path("login", UserLoginView.as_view(), name="login"),
    # Logout a user
    path("logout", UserLogoutView.as_view(), name="logout"),
    # Profile page for a user
    path("profile/<int:pk>", ProfileDetailView.as_view(), name="profile_detail"),
    # Update profile page for a user
    path("profile/update/<int:pk>", UpdateProfile.as_view(), name="profile_update"),
    # Password change
    path("profile/password-change", PasswordChangeView.as_view(), name="password_change")
]
