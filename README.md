# DW-42021-prj-grp1-Luangxay

This website is programmed using Django. It is a website to show the products you or others wish to sell. The program uses 4 applications

---
###Administration

This application allows people who have admin access, meaning that they are part of an administration group 
(admin_gp, admin_item_gp, admin_user_gp), to manage users. They can delete, warn, edit, add a user and specify their
group or modify it. Also, they can flag a user which highlights the user in the list.
There is also a log for the administration activities. Admin user can also edit the product of any users. 

---
###Item

This application allows users to add an item that they want to sell. It also allows others to like, comment, or flag (aka bookmark),
A user can filter the list of products by category or popularity (by the number of likes) or search for a specific product in a nav bar.
The user can also view additional details about the item itself and the seller.
The owner can delete and modify its product. 

---
###Messaging

Allows two registered user to talk to each other asynchronously and in real time. When a user receives a message, he will get a notification giving him the number
of messages he received. Once the link inbox is clicked, the notification disappear.

---
###User Management

User Management allows visitors to register, and members to login. They can also view their profile, edit it or change their password.
The items that they added to the website are also displayed on their page. 

---
###Installation Instructions:
1. pip install Pillow
2. pip install django-crispy-forms
3. pip install psycopg2
4. pip install channels
5. pip install django-shapeshifter
6. pip install django-heroku
7. pip install gunicorn
8. Create database in postgreSQL called "Product"
9. python manage.py migrate
10. python manage.py setup_db
11. Right click folder project2_products, go to Mark directory as, click on Mark as Source Root (do this step only if you are using pyCharm)

---

####Here is the link to the Deployment in Heroku: 
https://django-products.herokuapp.com/
